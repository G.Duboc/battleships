package com.company;

public class Battlefield {
    private char[][] battlefield;
    private int battlefieldSize;

    Battlefield(int size) {
        this.battlefieldSize = size;
        battlefield = new char[size][size];
        initBattlefield();
    }

    public char[][] getBattlefield() {
        return battlefield;
    }

    protected void initBattlefield() {
        for (int i = 0; i < battlefieldSize; i++) {
            for (int j = 0; j < battlefieldSize; j++) {
                battlefield[i][j] = ' ';
            }
        }
    }

    protected void initComputerBattlefield() {
        do {
            int a[] = getRandomShip();
            this.getBattlefield()[a[0]][a[1]] = '@';
        } while (numOfShipsOnBattelfield(this) < 5);
    }

    public static String printPlayerLine(char[] input, int i) {
        String result = i + " |";
        for (char c : input) {
            result += c;
        }
        result += "| " + i;
        return result;
    }

    public static String printEnemyLine(char[] input, int i) {
        String result = i + " |";
        for (char c : input) {
            if (c == '@') {
                result += ' ';
            } else {
                result += c;
            }
        }
        result += "| " + i;
        return result;
    }

    public String showBattlefield(Battlefield battlefield) {
        StringBuilder sb = new StringBuilder();
        sb.append("\\\\\\\\\\PLAYER/////" + "     |      \\\\\\\\OPPONENT////\n");
        for (int i = -1; i < battlefieldSize + 1; i++) {
            if (i == -1 || i == battlefieldSize) {
                sb.append("   0123456789   " + "     |        0123456789   \n");
            } else {
                sb.append(printPlayerLine(this.getBattlefield()[i], i) + "     |     " + printEnemyLine(battlefield.getBattlefield()[i], i) + "\n");
            }
        }
        sb.append("\\\\\\\\\\PLAYER/////" + "     |      \\\\\\\\OPPONENT////\n");
        return sb.toString();
    }

    public int numOfShipsOnBattelfield(Battlefield battlefield) {
        int counter = 0;
        for (int i = 0; i < battlefieldSize; i++) {
            for (int j = 0; j < battlefieldSize; j++) {
                if (battlefield.getBattlefield()[i][j] == '@')
                    counter++;
            }
        }
        return counter;
    }

    public void setShipOnMap(int a, int b) {
        this.getBattlefield()[a][b] = '@';
    }
    
    public void launchAttack(int a, int b) {
        if (battlefield[a][b] == '@') {
            battlefield[a][b] = 'X';
        } else if (battlefield[a][b] == ' ') {
            battlefield[a][b] = '-';
        }
    }

    public int[] getRandomShip() {
        int result[] = new int[2];
        result[0] = (int) (Math.random() * battlefieldSize);
        result[1] = (int) (Math.random() * battlefieldSize);
        return result;
    }

    public boolean checkWinCondition() {
        return (!(numOfShipsOnBattelfield(this) <= 0));
    }
}
