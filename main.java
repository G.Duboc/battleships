import java.util.Scanner;

public class Main {

    private static int BATTLEFIELD_SIZE = 10;

    public static void main(String[] args) {

        boolean isGameRunning = true;

        Battlefield playerBattlefield = new Battlefield(BATTLEFIELD_SIZE);
        Battlefield computerBattlefield = new Battlefield(BATTLEFIELD_SIZE);
        computerBattlefield.initComputerBattlefield();

        System.out.println("***********************************************");
        System.out.println("********* Welcome to the Battleship ! *********");
        System.out.println("***********************************************");
        System.out.println();

        askingForPositions(playerBattlefield);

        int[] coordinates = new int[2];
        int counter = 0;
        do {
            if (counter % 2 == 0) {
                System.out.println(playerBattlefield.showBattlefield(computerBattlefield));
                coordinates = getPlayerInput();
                computerBattlefield.launchAttack(coordinates[0], coordinates[1]);
            } else {
                coordinates = computerBattlefield.getRandomShip();
                playerBattlefield.launchAttack(coordinates[0], coordinates[1]);
            }

            if (!computerBattlefield.checkWinCondition()) {
                System.out.println("CONGRATS ! YOU WON !");
                break;
            } else if (!computerBattlefield.checkWinCondition()) {
                System.out.println("Bad luck, try again next Time !");
                break;
            }
            counter++;
        } while (isGameRunning);
    }


    public static void askingForPositions(Battlefield battlefield) {
        Scanner scanner = new Scanner(System.in);
        int aCoordinate = 0;
        int bCoordonate = 0;

        System.out.println("Please cap'tain, report the position of your 5 ships !");
        do {
            do {
                System.out.println("YOUR TURN !\nPlease enter the X coordinate :");
                aCoordinate = checkCoordinates(scanner.nextLine());
            } while (aCoordinate < 0);
            do {
                System.out.println("Please enter the Y coordinate :");
                bCoordonate = checkCoordinates(scanner.nextLine());
            } while (bCoordonate < 0);

            battlefield.setShipOnMap(aCoordinate, bCoordonate);
        }
        while (battlefield.numOfShipsOnBattelfield(battlefield) < 5);

    }

    public static int[] getPlayerInput() {
        Scanner scanner = new Scanner(System.in);
        int[] coordinates = new int[2];
        do {
            System.out.println("YOUR TURN !\nPlease enter the X coordinate :");
            coordinates[0] = checkCoordinates(scanner.nextLine());
        } while (coordinates[0] < 0);
        do {
            System.out.println("Please enter the Y coordinate :");
            coordinates[1] = checkCoordinates(scanner.nextLine());
        } while (coordinates[1] < 0);
        return coordinates;
    }

    public static int checkCoordinates(String input) {
        int result = -1;
        try {
            result = Integer.parseInt(input);
        } catch (Exception e) {
            System.out.println("Not a valid number !");
        }
        return result;
    }
}
